-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop procedure if exists `pro_score`;
create procedure pro_score(test_cid int)
begin
	declare avg_score decimal(5,3) default 0;
	declare max_score int default 0;
	declare min_score int default 0;
	declare num cursor for select avg(score),max(score),min(score) from sc where cid=test_cid;
	-- 打开游标
	open num;
	-- 使用游标
	fetch num into avg_score,max_score,min_score;
	-- 关闭游标
	close num;
	select avg_score as 平均分,max_score as 最高分,min_score as 最低分;
end;
call pro_score(1);
-- 
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
drop procedure if exists `pro_sid`;
delimiter $$
create procedure pro_sid(test_tid varchar(2))
begin
	declare te_sid varchar(20) default '';
	declare te_sname varchar(20) default '';
	declare te_run varchar(255) default '';
	declare flag int default true;
	declare num cursor for
	select sname,sid from student where sid in
		(
		select sid from sc where cid in
			(
			select cid from course where tid=test_tid
			)
		);
		declare continue handler for not found set flag = false;
		open num;
		aa:while flag do
		fetch num into te_sname,te_sid;
			if flag=false then
				leave aa;
			end if;
			if te_run='' then
				set te_run=concat(te_sname,'(',te_sid,')');
			else
				set te_run=concat(te_run,',',te_sname,'(',te_sid,')');
			end if;
		end while;
		close num;
		select te_run;
end;
$$
delimiter ;
call pro_sid('01');
-- 
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
drop procedure if exists `pro_cname`;
create procedure pro_cname(pa_sid int)
begin
	declare ts_sname varchar(20) default '';
	declare ts_cname varchar(20) default '';
	declare ts_score varchar(20) default '';
	declare ts_msg varchar(255) default '';
	declare flag int default true;
	declare num cursor for(
	select student.sname,course.cname,sc.score from sc
	inner join student on student.sid=sc.sid
	inner join course on course.cid=sc.cid
	where sc.sid=pa_sid);
	declare continue handler for not found set flag=false;
	open num;
			aa:while true do
	fetch num into ts_sname,ts_cname,ts_score;
			if flag=false then
				leave aa;
			end if;
			if ts_msg='' then
				set ts_msg=concat(ts_sname,'：',ts_cname,'(',ts_score,')');
			else
				set ts_msg=concat(ts_msg,',',ts_cname,'(',ts_score,')');
			end if;
			end while;
	close num;
	select ts_msg;
end;
call pro_cname(2);
-- 
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
-- 01
drop procedure if exists `pro_like`;
delimiter //
create procedure pro_like()
begin
	declare re_sid varchar(20) default '';-- 存学号
	declare re_sname varchar(20) default '';-- 存姓名
	declare k_char varchar(20) default '';-- 存替换后的值
	declare b int default 1;-- 范围号
	declare i int default 0;-- 统计该值出现的次数
	declare num varchar(255) default '';-- 存符合的人
	declare flag int default true;
	declare msg cursor for select sid,sname from student;
	declare continue handler for not found set flag=false;
	open msg;
	aa:while flag do
	fetch msg into re_sid,re_sname;
		if flag=false then
			leave aa;
		end if;
			set b=CHAR_LENGTH(re_sname);
			
			bb:while b>0 do
				set k_char=REPLACE(re_sname,SUBSTR(re_sname,b,1),'');
				set i=CHAR_LENGTH(re_sname)-CHAR_LENGTH(k_char);-- 算出被替换掉的值有多少
				if i>=2 then
					set num=concat(num,',(',re_sid,')',re_sname);
					leave bb;
				end if;
				set b=b-1;
			end while;
			
	end while;
	close msg;
	select substr(num,2);
end;
//
delimiter ;
call pro_like();
-- 02
drop procedure if exists `pro_pink`;
delimiter $$
create procedure pro_pink()
begin
	declare s_sid varchar(30) default '';
	declare s_sname varchar(30) default '';
	declare s_char varchar(1) default'';
	declare i int default 1;
	declare k int default 0;
	declare run varchar(255) default '';
	declare flag int default true;
	
	declare msg cursor for select sid,sname from student;
	declare continue handler for not found set flag =false;
	
	open msg;
	aa:while flag do
	fetch msg into s_sid,s_sname;
		if flag=false then
			leave aa;
		end if;
		set i=1;
		set k=char_length(s_sname);
		bb:while true do
			-- 当执行到等同长度时跳出
			if k=i then
				leave bb;
			end if;
			set s_char=substr(s_sname,i,1);
			
			if s_sname like concat('%',s_char,'%',s_char,'%') then
				set run=concat(run,',(',s_sid,')',s_sname);
				leave bb;
			end if;
			set i=i+1;
		end while;
		
	end while;
	close msg;
	select substr(run,2);
end;
$$
delimiter ;
call pro_pink();







