-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop procedure if exists `class`;
delimiter $$
create procedure class(num int)
   BEGIN
      declare a varchar(10) default '';
      declare b varchar(10) default '';
      declare c varchar(10) default '';
      declare nu cursor for
      (select avg(score),max(score),min(score)from sc where cid=num);
      open nu;
         fetch nu into a,b,c;
      close nu;
      select a,b,c;
   END
   $$
   delimiter ;
call class(01)

-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
drop procedure if exists `t_x`;
delimiter $$
create procedure t_x(a varchar(10))
 BEGIN
    declare t_sid varchar(10) default '';
    declare t_sname varchar(10)default '';
    declare fa int default true;
    declare n varchar(200) default '';
    declare mu cursor FOR(
    select sid,sname from student where sid in
      ((select sid from sc where cid=
         (select cid from teacher where tid=a)))
    );
    declare continue handler for not found set fa=false;
    open mu;
    aa:while true DO
         fetch mu into t_sid,t_sname;
         if fa=false THEN
           leave aa;
         end if;
         if n='' THEN
         set n=concat(t_sname,'(',t_sid,')');
         else
         set n=concat(n,',',t_sname,'(',t_sid,')');
         end if;
       end while;
    close mu;
    select n;
 end;
 $$
 delimiter ;
call t_x('02')
 
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
drop procedure if exists `stu_xx`;
delimiter $$
create procedure stu_xx(stu_i int)
BEGIN
   declare s_name varchar(10) default '';
   declare c_name varchar(10) default '';
   declare s_num varchar(10) default '';
   declare mm varchar(200) default '';
   declare fa int default true;
   declare dx cursor for
   (
      select student.sname,course.cname,sc.score from student inner join
      sc on student.sid=sc.sid inner join course on sc.cid=course.cid
      where sc.sid=stu_i
   );
   declare continue handler for not found set fa=false;
   open dx;
	 aa:while true do
	    fetch dx into s_name,c_name,s_num;
		  if fa=false then
			leave aa;
			end if;
			if mm=''then
		  set mm=concat(s_name,':',c_name,'(',s_num,')');
			else
			set mm=concat(mm,',',s_name,':',c_name,'(',s_num,')');
			end if;
		  end while;
   close dx;
	 select mm;
end;
$$
delimiter ;
call stu_xx(01)


 
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
drop procedure if exists `bx`;
delimiter $$
create procedure bx()
begin 
   declare stu_a varchar(10) default '';
   declare stu_b varchar(10) default '';
	 declare nu int default 0;
	 declare fa int default true;
	 declare ac cursor for
	 (
	    select sid,sname from student;
	 );
	 declare continue handler for not found set fa=false;
	 open ac;
	 fetch ac into stu_a,stu_b;
	 
	 close ac;
	 
   select sid,sname from student
end;
call work4();