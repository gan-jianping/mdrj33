```mysql
drop procedure if exists `work5`;
delimiter $$
create procedure `work5`()
begin
	declare type varchar(10) default '';
	declare num int default 0;
	declare flag int default true;
	declare mycur cursor for(
	select sname from student);

	declare CONTINUE handler for not found set flag=false;
	open mycur;
	WHILE flag DO
		fetch mycur into type;
		IF flag=true THEN
			set num = (select char_length(type));
			WHILE num>1 DO
				if mid(type,num,1)=mid(type,num-1,1) then
					select type;
				end if;
				set num=num-1;
			END WHILE;
		END IF;
	END WHILE;
end
$$
delimiter ;
call work5();
```

