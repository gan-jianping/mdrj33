use asdd

create TABLE Student
(
stuid int PRIMARY KEY,
stuName VARCHAR(20) not null,
stusj datetime not null,
stucj datetime 
);

SELECT name,LENGTH(name) from user;
select LENGTH('123ab中国c'),OCTET_LENGTH('123ab俄罗斯c')

select CHAR_LENGTH('123ab法国c')

-- 替换
select replace( '123425','2','ad')

select INSERT('12342562789',2,3,'ADC')

-- 截取
SELECT select('123488',3)

SELECT RIGHT('879546',3)

select SUBSTRing('123456789',-5,3)

-- 拼接
select concat('wer','bsb','4881') 

-- 逆序
select REVERSE('123444')

-- 大小写转换
select UPPER('美国eeeEFG')

select LOWER('法国GABsb')

-- 去空格
select TRIM('       13  2      ');

select LTRIM('       3   13     ');

select RTRIM('       14   5        ');

select REPLACE('    15  6      ',' ','');

-- 时间函数
select NOW()

-- 流程控制
-- IF 三目运算符
select *,IF(stuid>=3,'小狗','小猫')改名 from Student;

select *,
(
 CASE 
WHEN stuid>=3 THEN
		'小狗'
ELSE
		'小猫'
END 
)改名
 from Student;
