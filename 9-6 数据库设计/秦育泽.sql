/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/6 23:06:58                            */
/*==============================================================*/


drop table if exists AreaInfo;

drop table if exists AreaPersonnelTable;

drop table if exists ElectricityGradientTable;

drop table if exists MaintenanceInfoTable;

drop table if exists MaintenancePersoTable;

drop table if exists UserAccountTable;

drop table if exists electricity_classification_table;

drop index UseID on user_info;

drop table if exists user_info;

/*==============================================================*/
/* Table: AreaInfo                                              */
/*==============================================================*/
create table AreaInfo
(
   AreaID               int not null,
   AreaName             varchar(60),
   Profitability        decimal(20,2),
   RegionalElectricityConsumption int,
   RegionalHead         varchar(60),
   primary key (AreaID)
);

/*==============================================================*/
/* Table: AreaPersonnelTable                                    */
/*==============================================================*/
create table AreaPersonnelTable
(
   AreaID               int,
   Personnel            int,
   Personnelphone       varchar(0)
);

/*==============================================================*/
/* Table: ElectricityGradientTable                              */
/*==============================================================*/
create table ElectricityGradientTable
(
   UserID               int not null,
   GradientGrade        varchar(3) not null,
   primary key (GradientGrade)
);

/*==============================================================*/
/* Table: MaintenanceInfoTable                                  */
/*==============================================================*/
create table MaintenanceInfoTable
(
   MaintenancePersonnelID int,
   MaintenanceTimes     int,
   EquipmentMaintenanceRecords text
);

/*==============================================================*/
/* Table: MaintenancePersoTable                                 */
/*==============================================================*/
create table MaintenancePersoTable
(
   MaintenancePersonnelID int not null,
   MPName               varchar(60),
   MPPhone              varchar(20),
   state                varchar(1),
   primary key (MaintenancePersonnelID)
);

/*==============================================================*/
/* Table: UserAccountTable                                      */
/*==============================================================*/
create table UserAccountTable
(
   �û�ID                 int,
   UserPowerGradient    int,
   ElectricityConsumption decimal(20.2),
   "Recharge electricityBillThisMonth" decimal(20,2),
   BalanceThisMonth     decimal(20,2)
);

/*==============================================================*/
/* Table: electricity_classification_table                      */
/*==============================================================*/
create table electricity_classification_table
(
   UserID               int not null,
   HouseholdElectricity varchar(1),
   LargeIndustrialElectricity varchar(1),
   primary key (UserID)
);

/*==============================================================*/
/* Table: user_info                                             */
/*==============================================================*/
create table user_info
(
   UserID               int not null auto_increment,
   UserAccount          varbinary(20),
   UserPassword         varchar(20),
   UserName             varchar(60),
   UserIdNumber         varchar(18),
   UserAdress           varchar(60),
   PowerGradient        int,
   UserPhoneNumber      varchar(20),
   UserBalance          decimal(20,2),
   AreaID               int,
   MaintenancePersonnelID int,
   ISArrears            int,
   CreteTime            timestamp,
   UpTime               timestamp,
   primary key (UserID)
);

/*==============================================================*/
/* Index: UseID                                                 */
/*==============================================================*/
create unique index UseID on user_info
(
   UserID
);

alter table AreaPersonnelTable add constraint FK_Reference_6 foreign key (AreaID)
      references AreaInfo (AreaID) on delete restrict on update restrict;

alter table ElectricityGradientTable add constraint FK_Reference_2 foreign key (UserID)
      references user_info (UserID) on delete restrict on update restrict;

alter table MaintenanceInfoTable add constraint FK_Reference_7 foreign key (MaintenancePersonnelID)
      references MaintenancePersoTable (MaintenancePersonnelID) on delete restrict on update restrict;

alter table UserAccountTable add constraint FK_Reference_8 foreign key (�û�ID)
      references user_info (UserID) on delete restrict on update restrict;

alter table electricity_classification_table add constraint FK_Reference_1 foreign key (UserID)
      references user_info (UserID) on delete restrict on update restrict;

alter table user_info add constraint FK_Reference_3 foreign key (PowerGradient)
      references ElectricityGradientTable (GradientGrade) on delete restrict on update restrict;

alter table user_info add constraint FK_Reference_4 foreign key (AreaID)
      references AreaInfo (AreaID) on delete restrict on update restrict;

alter table user_info add constraint FK_Reference_5 foreign key (MaintenancePersonnelID)
      references MaintenancePersoTable (MaintenancePersonnelID) on delete restrict on update restrict;

