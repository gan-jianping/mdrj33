
-- select 
-- 1.查询所有用户信息以及卡数量
select *,(select count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId)卡数量
from accountinfo;

-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select *,(select count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId)卡数量,
(select sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId)总余额
from accountinfo;


select accountinfo.*,a.卡数量,a.总余额
from(select AccountId,count(*)卡数量,sum(CardMoney)总余额 from bankcard group by AccountId)a
inner join accountinfo on accountinfo.AccountId=a.AccountId;
-- where 
-- 3.查询余额最高的账户
select AccountId,sum(CardMoney)money from bankcard group by AccountId order by money desc limit 1;


select AccountId,max(money) from 
(select AccountId,sum(CardMoney) as money from bankcard group by AccountId)a
order by a.money desc limit 1;


select * from accountinfo
where AccountId =
(
	select AccountId from
		(
			select AccountId,sum(CardMoney)money from bankcard group by AccountId order by money DESC limit 1
		)a
);


-- 4.查询出其他储户余额比关羽某张银行卡(6225098234235)多的银行卡号，显示卡号，身份证，姓名，余额
select a.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,a.CardMoney 余额 from accountinfo inner join
(select AccountId,CardNo,CardMoney from bankcard where CardMoney>(select CardMoney from bankcard where CardNo='6225098234235')
)a on accountinfo.AccountId=a.AccountId where accountinfo.AccountId != 3;



-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
select * from cardexchange where CardId in
(select CardId from bankcard where AccountId=
  (
    select AccountId from 
    (
     select AccountId,sum(CardMoney)money from bankcard
     group by AccountId order by money desc limit 1
    )a
  )
)


select * from cardexchange where CardId in(
	select CardId from bankcard where AccountId = 
	(
		select AccountId from 
		(
			select AccountId,sum(CardMoney) as money from bankcard 
			GROUP BY AccountId
			ORDER BY money desc
			LIMIT 1
		)temp
	)
)

-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额


-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额



-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额



-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称


-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数