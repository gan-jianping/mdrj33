
-- select 
-- 1.查询所有用户信息以及卡数量
select accountinfo.*,type.卡数量 from accountinfo inner join 
(select AccountId,count(*) 卡数量 from bankcard group by AccountId)type
on accountinfo.AccountId=type.AccountId
-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select accountinfo.*,type.卡数量,type.总余额 from accountinfo inner join 
(select AccountId,count(*) 卡数量,sum(CardMoney) 总余额 from bankcard group by AccountId)type
on accountinfo.AccountId=type.AccountId
-- where 
-- 3.查询余额最高的账户
select * from bankcard where CardMoney=(select max(CardMoney) from bankcard)
-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号，显示卡号，身份证，姓名，余额
select CardNo,AccountCode,RealName,CardMoney from accountinfo inner join 
(
select * from bankcard where CardMoney>(select CardMoney from bankcard where CardNo='6225098234235')
)type on accountinfo.AccountId=type.AccountId
-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
select * from cardexchange where CardId in (select CardId from bankcard where CardMoney=(select max(CardMoney) from bankcard))
-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select CardNo,AccountCode,RealName,CardMoney from accountinfo inner join(
select * from bankcard where CardId in(select CardId from cardexchange where MoneyOutBank>0)
)type on accountinfo.AccountId=type.AccountId
-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select * from accountinfo left join (
select * from bankcard where CardId not in (
select CardId from cardexchange where MoneyInBank>0
)
)type on accountinfo.AccountId=type.AccountId
-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select CardNo,AccountCode,RealName,CardMoney from accountinfo inner join (
select * from bankcard where CardId not in (select CardIdOut from cardtransfer) 
and CardId not in(select CardIdIn from cardtransfer)
)type on accountinfo.AccountId=type.AccountId
-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
select type.*,accountinfo.RealName from accountinfo inner join(
select * from bankcard where CardMoney>100
)type on accountinfo.AccountId=type.AccountId;
-- HAVING
-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
select CardNo,AccountCode,RealName,CardMoney,(
select count(*) from cardexchange where cardid=(select CardId from cardexchange group by CardId order by count(*) desc limit 1)
)交易次数
from accountinfo inner join (
select * from bankcard where CardId = (
select CardId from cardexchange group by CardId order by count(*) desc limit 1)
)type on accountinfo.AccountId=type.AccountId



