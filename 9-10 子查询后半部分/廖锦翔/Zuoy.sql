
-- select 
-- 1.查询所有用户信息以及卡数量
SELECT *,(
SELECT COUNT(*) FROM bankcard WHERE bankcard.AccountId=accountinfo.AccountId
)
FROM accountinfo


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
SELECT *,
(
SELECT COUNT(*) FROM bankcard WHERE bankcard.AccountId=accountinfo.AccountId
)卡数量,
(
select sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId
)总余额
FROM accountinfo

-- where 
-- 3.查询余额最高的账户
select accountinfo.*,bankcard.CardMoney from accountinfo
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
where CardMoney>=(SELECT max(CardMoney) from bankcard)


-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号，显示卡号，身份证，姓名，余额

select * from 
(
select   bankcard.AccountId,bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
where CardMoney>(SELECT CardMoney from bankcard where CardNo='6225098234235')
) dddd
where dddd.AccountId!=(SELECT AccountId from bankcard where CardNo='6225098234235')

-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）

select * from cardexchange where CardId in
(
	select CardId from bankcard where AccountId = 
	(
		select AccountId from 
		(
			select AccountId,sum(CardMoney) as money from bankcard 
			GROUP BY AccountId
			ORDER BY money desc
			LIMIT 1
		)temp
	)
)
-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select bankcard.CardNo, accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
INNER JOIN bankcard on accountinfo.AccountId=bankcard.AccountId
where CardId in 
(select CardId from cardexchange where MoneyInBank!=0)

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select bankcard.CardNo, accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
INNER JOIN bankcard on accountinfo.AccountId=bankcard.AccountId
where CardId  not in 
(select CardId from cardexchange where MoneyInBank!=0)


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select bankcard.CardNo, accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
INNER JOIN bankcard on accountinfo.AccountId=bankcard.AccountId
where CardId in 
(
select CardId from cardexchange where CardMoney>0 and MoneyOutBank>0
)


-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
select RealName,bankcard.* from accountinfo
INNER JOIN bankcard on accountinfo.AccountId=bankcard.AccountId
where CardMoney>100

-- HAVING
-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数

select bankcard.CardNo, accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney,ddddd.money from accountinfo
INNER JOIN bankcard on accountinfo.AccountId=bankcard.AccountId
inner join (
select CardId,COUNT(*)as money from cardexchange
GROUP BY CardId
ORDER BY money DESC
LIMIT 1
)ddddd on ddddd.CardId=bankcard.CardId