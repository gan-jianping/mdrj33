-- 过程：
-- 1.输入教师名称，查询学过该教师课程的学生姓名
--   如果人数超过3人，则打印出大班；否则小班；
drop PROCEDURE if EXISTS `Lesson1`;
create procedure Lesson1(in par_tname varchar(10))
	begin
		IF par_tname ="张三" THEN
			select count(*) into @num1 from student where sid in(
				select sid from sc where cid in(
					select cid from course where tid =(
						select tid from teacher where tname="张三"
					)
				)
			);
		IF @num1 > 3 THEN
			select "大班";
		ELSE
			select "小班";
		END IF;

		ELSEIF par_tname="李四" THEN
			select count(*) into @num2 from student where sid in(
			select sid from sc where cid in(
				select cid from course where tid =(
					select tid from teacher where tname="李四"
				)
			)
		);
	IF @num2>3 THEN
		select "大班";
	ELSE
		select "小班";
	END IF;	

ELSEIF par_tname="王五" THEN
	select count(*) into @num3 from student where sid in(
		select sid from sc where cid in(
			select cid from course where tid =(
				select tid from teacher where tname="王五"
			)
		)
	);
		IF @num3>3 THEN
			select "大班";
		ELSE
			select "小班";
		END IF;	
	ELSE
		SELECT "查无此教师";
	END IF;
end;

call Lesson1("张三");

-- 2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
DROP PROCEDURE if EXISTS `Lesson2`;
create PROCEDURE Lesson2(classid int)
  BEGIN
		DECLARE Excellent DECIMAL(5,2) DEFAULT 0;
		DECLARE good DECIMAL(5,2) DEFAULT 0;
		IF classid = 1 THEN
			set Excellent  = (select count(*) from sc where score>=80 and cid =1)/(select count(*) from sc where cid=1);
			set good  = (select count(*) from sc where score>=70 and score<80 and cid = 1)/(select count(*) from sc where cid=1) ;
			if Excellent>good then
				select "学霸班";
			else
				select "普通班";
			end if;

		elseIF classid = 2 THEN
			set Excellent  = (select count(*) from sc where score>=80 and cid =2)/(select count(*) from sc where cid=2);
			set good  = (select count(*) from sc where score>=70 and score<80 and cid = 2)/(select count(*) from sc where cid=2) ;
			if Excellent>good then
				select "学霸班";
			else
				select "普通班";
			end if;

		elseIF classid = 3 THEN
			set Excellent  = (select count(*) from sc where score>=80 and cid =3)/(select count(*) from sc where cid=3);
			set good  = (select count(*) from sc where score>=70 and score<80 and cid = 3)/(select count(*) from sc where cid=3) ;
			if Excellent>good then
				select "学霸班";
			else
				select "普通班";
			end if;
	ELSE 
		SELECT "查无此班";
	END if;
END;

call Lesson2(3)



-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1
drop PROCEDURE if EXISTS `Lesson3`;
create PROCEDURE Lesson3(num int)
	BEGIN
		DECLARE sum int DEFAULT 1;
		WHILE num>0 DO
			set sum = sum*num;
			set num = num-1;
		END WHILE;
		SELECT sum;
END;

call Lesson3(5)
-- 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）

select *,left(sage,4) as 年 from student 








