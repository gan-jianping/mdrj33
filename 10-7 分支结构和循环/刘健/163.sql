drop procedure if exists `class`
create procedure class(Nteacher varchar(20) )
begin
	declare sum int default 0;
	set sum =
	(
		select count(*) from student where sid in
		(
			select sid from sc where cid=
			(
				select cid from course where tid=
				( 
					select tid from teacher where tname=Nteacher
				)
			)
		)
	);
	if sum>3 then
	select '大班';
	else
	select '小班';
	end if;
end
call class();


drop procedure if exists `look`;
create procedure look(id int)
begin
	declare num default 0;
	declare sum default 0;
	
	set num=(select (count(IF(score>=90,score,null))/count(*)) 优秀率 from sc GROUP BY cid having cid = id);
  set sum=(select (count(if(score>=80 and score<90,score, null))/count(*))优良率 from sc GROUP BY cid having cid=01);
	 if num>sum THEN
   select '学霸班';
   else 
   select '普通班';
   end if;
end;
call look();


drop procedure if exists `mach`;
create procedure mach(num int)
begin 
   declare sum int default 1;
while num>0 DO
   set sum=sum*num;
   set num=num-1;
end while;
   select sum;
end;
call mach(5);



drop procedure if exists `stime`;
create procedure stime(y int)
begin
   if 1985<=y and y<=1989 then
	 select * from student where year(sage)=y;
	 elseif 1991<=y and y<=1995 then
	 select * from student where year(sage)=y;
	 else
	 select('不在时间范围内');
	 end if;
end;
   call stime();
