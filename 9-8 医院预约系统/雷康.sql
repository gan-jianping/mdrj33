/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/8 11:34:56                            */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_专家信息表_REFERENCE_科室表') then
    alter table 专家信息表
       delete foreign key FK_专家信息表_REFERENCE_科室表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_出诊表_REFERENCE_专家信息表') then
    alter table 出诊表
       delete foreign key FK_出诊表_REFERENCE_专家信息表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约表_REFERENCE_用户表') then
    alter table 预约表
       delete foreign key FK_预约表_REFERENCE_用户表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约表_REFERENCE_专家信息表') then
    alter table 预约表
       delete foreign key FK_预约表_REFERENCE_专家信息表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约表_REFERENCE_科室表') then
    alter table 预约表
       delete foreign key FK_预约表_REFERENCE_科室表
end if;

drop table if exists 专家信息表;

drop table if exists 出诊表;

drop table if exists 用户表;

drop table if exists 科室表;

drop table if exists 预约表;

/*==============================================================*/
/* Table: 专家信息表                                                 */
/*==============================================================*/
create table 专家信息表 
(
   id                   int                            not null,
   name                 varchar(11)                    null,
   sex                  bit                            null,
   department           int                            null,
   skill                text                           null,
   "time"               date                           null,
   Column_7             char(10)                       null,
   constraint PK_专家信息表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 出诊表                                                   */
/*==============================================================*/
create table 出诊表 
(
   出诊编号id               int                            not null,
   id                   int                            null,
   出诊医生sex              bit                            null,
   特长skill              text                           null,
   constraint PK_出诊表 primary key clustered (出诊编号id)
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表 
(
   id                   int                            not null,
   name                 varchar(20)                    null,
   sex                  bit                            null,
   number               varchar(20)                    null,
   phone                varchar(11)                    null,
   password             varchar(20)                    null,
   password2            varchar(20)                    null,
   "add"                text                           null,
   constraint PK_用户表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 科室表                                                   */
/*==============================================================*/
create table 科室表 
(
   id                   int                            not null,
   name                 varchar(20)                    null,
   constraint PK_科室表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 预约表                                                   */
/*==============================================================*/
create table 预约表 
(
   id                   int                            not null,
   name                 int                            null,
   doctor               int                            null,
   department           int                            null,
   "time"               datetime                       null,
   constraint PK_预约表 primary key clustered (id)
);

alter table 专家信息表
   add constraint FK_专家信息表_REFERENCE_科室表 foreign key (department)
      references 科室表 (id)
      on update restrict
      on delete restrict;

alter table 出诊表
   add constraint FK_出诊表_REFERENCE_专家信息表 foreign key (id)
      references 专家信息表 (id)
      on update restrict
      on delete restrict;

alter table 预约表
   add constraint FK_预约表_REFERENCE_用户表 foreign key (name)
      references 用户表 (id)
      on update restrict
      on delete restrict;

alter table 预约表
   add constraint FK_预约表_REFERENCE_专家信息表 foreign key (doctor)
      references 专家信息表 (id)
      on update restrict
      on delete restrict;

alter table 预约表
   add constraint FK_预约表_REFERENCE_科室表 foreign key (department)
      references 科室表 (id)
      on update restrict
      on delete restrict;

