CREATE TABLE `ExpertsInfo` (
`ExpertId` int NOT NULL AUTO_INCREMENT COMMENT '专家序号',
`name` varchar(20) NOT NULL COMMENT '专家姓名',
`sex` char(1) NOT NULL COMMENT '专家性别',
`Section_id` int NOT NULL COMMENT '科室类别',
`Category_id` varchar(20) NOT NULL COMMENT '科室名称',
`Position_name` varchar(20) NOT NULL COMMENT '职务名称',
`Intro` varchar(100) NOT NULL COMMENT '特长概述\\个人简介',
`CreateTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
`VisitsTime` time NOT NULL COMMENT '出诊时间',
PRIMARY KEY (`ExpertId`) 
);

CREATE TABLE `SectionInfo` (
`Sectionid` int NOT NULL AUTO_INCREMENT COMMENT '科室类别序号',
`Section_name` varchar(20) NOT NULL COMMENT '科室类别名称',
`CreateTime` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
`ModifyTime` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
PRIMARY KEY (`Sectionid`) ,
UNIQUE INDEX (`Section_name` ASC)
);

CREATE TABLE `UserInfo` (
`uid` int NOT NULL AUTO_INCREMENT COMMENT '用户序号',
`uname` varchar(30) NOT NULL COMMENT '用户名字',
`usex` char(1) NOT NULL COMMENT '用户性别',
`ucard` varchar(18) NOT NULL COMMENT '用户身份号码',
`uphone` int(11) NOT NULL COMMENT '用户手机号',
`upwd` varchar(16) NOT NULL DEFAULT 123456 COMMENT '用户密码',
`uaddress` varchar(50) NOT NULL COMMENT '用户住址',
`CreateTime` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
`ModifyTime` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
PRIMARY KEY (`uid`) ,
UNIQUE INDEX `all` (`ucard` ASC, `uphone` ASC) COMMENT '身份证号码和手机号码是唯一的' 
);

CREATE TABLE `RegistrationInfo` (
`id` int NOT NULL AUTO_INCREMENT,
`Sectionid` int NOT NULL COMMENT '挂号科室类别',
`Categoryid` int NOT NULL COMMENT '挂号科室',
`Expertid` int NOT NULL COMMENT '专家编号',
`ExpertName` varchar(20) NOT NULL COMMENT '专家姓名',
`ExpertSex` char(1) NOT NULL COMMENT '专家性别',
`Postition_name` varchar(20) NOT NULL COMMENT '职务名称',
`Intro` varchar(100) NOT NULL COMMENT '特长概述',
`max_num` int(40) NOT NULL COMMENT '最大预约人数',
`reserve_time` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '预约时间',
`CreateTime` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
`ModifyTime` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
PRIMARY KEY (`id`) 
);

CREATE TABLE `Category` (
`Categoryid` int NOT NULL AUTO_INCREMENT COMMENT '科室序号',
`Section_name` varchar(20) NULL COMMENT '科室类别名称',
`Category_name` varchar(30) NULL COMMENT '科室名称',
`CreateTime` datetime NULL ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`Categoryid`) ,
UNIQUE INDEX (`Category_name` ASC)
);


ALTER TABLE `Category` ADD FOREIGN KEY (`Section_name`) REFERENCES `SectionInfo` (`Section_name`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `ExpertsInfo` ADD FOREIGN KEY (`Section_id`) REFERENCES `SectionInfo` (`Sectionid`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `ExpertsInfo` ADD FOREIGN KEY (`Category_id`) REFERENCES `Category` (`Categoryid`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `RegistrationInfo` ADD FOREIGN KEY (`Sectionid`) REFERENCES `SectionInfo` (`Sectionid`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `RegistrationInfo` ADD FOREIGN KEY (`Categoryid`) REFERENCES `Category` (`Categoryid`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `RegistrationInfo` ADD FOREIGN KEY (`Expertid`) REFERENCES `ExpertsInfo` (`ExpertId`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `RegistrationInfo` ADD FOREIGN KEY (`ExpertName`) REFERENCES `ExpertsInfo` (`name`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `RegistrationInfo` ADD FOREIGN KEY (`ExpertSex`) REFERENCES `ExpertsInfo` (`sex`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `RegistrationInfo` ADD FOREIGN KEY (`Postition_name`) REFERENCES `ExpertsInfo` (`Position_name`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `RegistrationInfo` ADD FOREIGN KEY (`Intro`) REFERENCES `ExpertsInfo` (`Intro`) ON DELETE RESTRICT ON UPDATE RESTRICT;

